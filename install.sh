#!/bin/sh

export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"
export LANGUAGE="en_US.UTF-8"

# Install NixOS
sh <(curl -L https://nixos.org/nix/install) --no-daemon

# Source NixOS
source $HOME/.nix-profile/etc/profile.d/nix.sh

# Install Nix packages
nix-env -iA \
	nixpkgs.bat \
	nixpkgs.btop \
    nixpkgs.diff-so-fancy \
	nixpkgs.docker \
	nixpkgs.docker-compose \
    nixpkgs.dua \
	nixpkgs.fnm \
    nixpkgs.gping \
	nixpkgs.ncdu \
	nixpkgs.neovim \
    nixpkgs.ripgrep \
	nixpkgs.stow \
    nixpkgs.tldr \
    nixpkgs.tre-command \
	nixpkgs.tree \
	nixpkgs.fira-code
	# nixpkgs.nodePackages.yarn \
	# nixpkgs.nodePackages.pnpm

# Install NodeJS versions
fnm install lts/fermium # v14
fnm install lts/gallium # v16
fnm install lts/hydrogen # v18
fnm install v19
fnm default lts/hydrogen
corepack enable # Yarn
corepack prepare pnpm@latest --activate # PNPM

sudo cp $HOME/.nix-profile/etc/systemd/system/* /etc/systemd/system
sudo groupadd docker
sudo usermod -aG docker ${USER}
sudo systemctl enable --now docker.service

# Install ZPlug
if [[ ! -d "$HOME/.zplug" ]]; then
	curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
fi

# Clean up
if [[ -e "$HOME/.zshrc" ]]; then
	rm $HOME/.zshrc
fi

# Dotfiles
if [[ -d "$HOME/.dotfiles" ]]; then
	cd $HOME/.dotfiles
	git pull
else
	git clone https://gitlab.com/surreira/dotfiles.git $HOME/.dotfiles
	cd $HOME/.dotfiles
fi

stow zsh spaceship git tmux neovim 

