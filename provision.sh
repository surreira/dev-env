#!/bin/sh

sudo apt update -y
sudo apt upgrade -y

sudo apt install -y \
	software-properties-common \
	build-essential \
	fonts-powerline \
	zsh-common \
	curl \
	zsh \
	git

sudo chsh -s $(which zsh) $USER

curl -sSL https://gitlab.com/surreira/dev-env/-/raw/main/install.sh | zsh
